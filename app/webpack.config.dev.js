const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.config');
const Dotenv = require('dotenv-webpack');

module.exports = merge(commonConfig, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        historyApiFallback: true,
    },
    plugins: [
        new Dotenv()
    ]
});
