import express from 'express';
import path from 'path';

const app = express();

const BUILD_PATH = path.join(__dirname, '../dist');

app.use(express.static(BUILD_PATH));
app.get('*', (_, res) => {
    res.sendFile(path.resolve(BUILD_PATH, 'index.html'));
});

app.listen(8000, () => {
    console.log(`⚡️[server]: Server is running on port: ${8000}`);
});
