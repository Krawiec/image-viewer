import { createApi, OrderBy } from 'unsplash-js';
import { PaginationParams } from 'unsplash-js/dist/types/request';

const unsplashApiClient = createApi({
    accessKey: process.env.CLIENT_ID!, // I know, I know... Time pressure :)
});

const getPhotos = async (params: PaginationParams, signal?: AbortSignal) =>
    await unsplashApiClient.photos.list(params, { signal });

const getLatestTenPhotos = async (signal?: AbortSignal) =>
    await getPhotos({ orderBy: OrderBy.LATEST, page: 0, perPage: 10 }, signal);

const getTopics = async () => await unsplashApiClient.topics.list({});

const getTopicPhotos = async (
    topicIdOrSlug: string,
    params: PaginationParams,
) => await unsplashApiClient.topics.getPhotos({ topicIdOrSlug, ...params });

const getPhoto = async (photoId: string, signal?: AbortSignal) =>
    await unsplashApiClient.photos.get({ photoId }, { signal });

export { getPhotos, getLatestTenPhotos, getTopics, getTopicPhotos, getPhoto };
