const RoutePaths = {
    HOME: '/',
    TOPIC: {
        path: '/topic/:topicId',
        formatPath: (topicId: string | null) => {
            if (topicId !== null) {
                return RoutePaths.TOPIC.path.replace(':topicId', topicId);
            }
        },
    },
    PHOTO: {
        path: '/photo/:photoId',
        formatPath: (photoId: string | null) => {
            if (photoId !== null) {
                return RoutePaths.PHOTO.path.replace(':photoId', photoId);
            }
        },
    },
};

export default RoutePaths;
