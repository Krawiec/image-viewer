export type ThemeType = typeof light;

export const light = {
    colors: {
        primary: '#050970a6',
        background: '#eee',
        shadowed: '#46464645',
        white: 'white',
        disable: '#04064e24',
        hover: '#46464645',
        black: 'black'
    },
    font: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        color: 'black',
        shadowed: 'rgba(0, 0, 0, 0.84)',
        small: '12px',
        medium: '16px',
        large: '25px'
    },
    paragraph: {
        fontSize: '0.875rem',
        fontWeight: 400,
        color: 'rgba(0, 0, 0, 0.54)'
    },
    boxShadows: {
        primary: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        hover: '5px 5px 9px 3px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)'
    },
    button: {
        boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 0px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
        active: '#edeaea'
    }
}

// export const dark: ThemeType = {
//     colors: {
//         primary: "#f45511",
//         background: "#eee",
//         shadowed: '#464646',
//         white: '#000'
//     },
//     font: {
//         fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
//         color: 'black',
//         shadowed: 'rgba(0, 0, 0, 0.54)',
//         small: '10px',
//         medium: '16px',
//         large: '25px'
//     },
//     paragraph: {
//         fontSize: '0.875rem',
//         fontWeight: 400,
//         color: 'rgba(0, 0, 0, 0.54)'
//     },
//     boxShadows: {
//         primary: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
//         hover: '5px 5px 9px 3px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)'
//     }
// }

const theme = light;
export default theme;