import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    * {
        border: 0;
        box-sizing: inherit;
        font-weight: inherit;
        margin: 0;
        outline: 0;
        padding: 0;
        text-decoration: none;
    }
    
    a {
        text-decoration: none;
    }
`

export default GlobalStyle;