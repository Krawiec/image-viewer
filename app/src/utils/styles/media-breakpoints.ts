export const MediaBreakpoints = {
    extraSmallDevice: '600px',
    mediumDevice: '768px',
    largeDevice: '992px',
    extraLargeDevice: '1200px',
};
