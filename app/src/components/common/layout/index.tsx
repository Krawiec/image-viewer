import React from 'react';
import GlobalStyle from '../../../utils/styles/global';
import AppBar from '../app-bar';

const Layout: React.FC = ({ children }) => {
    return (
        <div>
            <GlobalStyle />
            <AppBar />
            <div>
                {children}
            </div>
        </div>
    )
}

export default Layout;