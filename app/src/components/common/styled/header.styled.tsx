import styled from 'styled-components';

export const Header = styled.h1`
    text-align: center;
    font-family: ${props => props.theme.font.fontFamily};
    color: ${props => props.theme.font.shadowed};
    margin-bottom: 10px;
`;
