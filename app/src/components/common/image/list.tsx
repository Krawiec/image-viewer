import React from 'react';
import { Link } from 'react-router-dom';
import { PhotoPreview } from '../../../types/photo';
import RoutePaths from '../../../utils/routing';
import { ImagesWrapper, ImageWrapper } from '../../home/latest-images/styled';
import Photo from '.';
import Tooltip from '../tooltip';

interface IProps {
    photos: PhotoPreview[]
}

const ImagesList = ({ photos }: IProps) => {
    return (
        <ImagesWrapper>
            {photos.map((photo, idx) => (
                <Tooltip
                    text={`Likes: ${photo.likes.toString()}`}
                    key={idx}
                >
                    <ImageWrapper>
                        <Link
                            to={RoutePaths.PHOTO.formatPath(photo.id)!}
                        >
                            <Photo src={photo.urls.regular} />
                        </Link>
                    </ImageWrapper>
                </Tooltip>))
            }
        </ImagesWrapper>
    )
}

export default ImagesList;