import React, { memo } from 'react';
import ImageErrorWrapper from './image-error-wrapper';
import LazyLoad from 'react-lazyload';
import { Placeholder, StyledImage } from './styled';

interface IProps {
    src: string;
}

const Image = ({ src }: IProps) => {
    return (
        <ImageErrorWrapper
            render={(onFailed) => (
                <LazyLoad
                    style={{ height: '100%', width: '100%' }}
                    once={true}
                    placeholder={<Placeholder />}
                >
                    <StyledImage
                        src={src}
                        onError={onFailed} />
                </LazyLoad>
            )}
        />
    );
}

export default memo(Image);