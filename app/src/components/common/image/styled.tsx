import styled from 'styled-components';

interface IImageProps {
    visible?: boolean;
}

export const StyledImage = styled.img<IImageProps>`;
    object-fit: cover;
    display: ${({ visible = true }) => visible ? 'block' : 'none'};
    height: 100%;
    width: 100%;
    cursor:  pointer;
    transition: box-shadow .5s;
`;

export const Placeholder = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: inherit;
    height: inherit;
    width: 100%;
    height: 100%;
    background: ${props => props.theme.colors.shadowed};
`;