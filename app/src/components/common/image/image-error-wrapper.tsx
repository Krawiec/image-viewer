import React, { useState } from 'react';
import { Placeholder } from './styled';

interface IProps {
    render: (onError: () => void) => JSX.Element;
}

const ImageErrorWrapper = ({ render }: IProps) => {
    const [error, setIsError] = useState(false);

    const onError = () => setIsError(true);

    return (
        <>
            {error ? (
                <Placeholder>
                    FAILED
                </Placeholder>
            ) : (render(onError))}
        </>
    )
}

export default ImageErrorWrapper;