import React from 'react';
import { HorizontalCenter, Loader as StyledLoader, LoaderContainer } from './styled';

interface IProps {
    loading: boolean;
    children?: React.ReactNode;
}

const Loader = ({ loading, children }: IProps) => {
    return loading ? (
        <HorizontalCenter>
            <LoaderContainer>
                <StyledLoader />
            </LoaderContainer>
        </HorizontalCenter>
    ) : <>{children}</>;
}

export default Loader;