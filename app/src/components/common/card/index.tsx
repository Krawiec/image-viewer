import React from 'react';
import { CardContainer, CardHeader, CardDescription, CardImage, CardTitle, CircleAvatar } from './styled';
import Image from '../image';

interface IProps {
    title: string;
    description: string;
    imgSrc: string
}

const Card = ({
    title,
    description,
    imgSrc
}: IProps) => {
    return (
        <CardContainer>
            <CardHeader>
                <CircleAvatar>{title.slice(0, 1)}</CircleAvatar>
                <CardTitle>{title}</CardTitle>
            </CardHeader>
            <CardImage>
                <Image src={imgSrc} />
            </CardImage>
            <CardDescription>
                {description}
            </CardDescription>
        </CardContainer>
    )
}

export default Card;