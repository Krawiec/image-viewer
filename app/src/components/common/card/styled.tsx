import styled from 'styled-components';
import { MediaBreakpoints } from '../../../utils/styles/media-breakpoints';

export const CardContainer = styled.div`
    color: ${props => props.theme.font.color};
    border: none;
    cursor: default;
    width: 350px;
    height: 500px;
    outline: 0;
    margin: 10px;
    padding: 10px 0;
    box-sizing: border-box;
    transition: box-shadow .5s, height .5s, width .5s;
    align-items: center;
    font-family: ${props => props.theme.font.fontFamily};
    white-space: nowrap;
    border-radius: 16px;
    vertical-align: middle;
    text-decoration: none;
    background-color: ${props => props.theme.colors.white};
    box-shadow: ${props => props.theme.boxShadows.primary};
    overflow: hidden;
    cursor: pointer;

    @media screen and (max-width: ${MediaBreakpoints.extraLargeDevice}) {
        width: 250px;
        height: 300px;
    }
    
    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        height: 150px;
        width: 200px;
    } 

    &:hover {
        box-shadow: ${props => props.theme.boxShadows.hover};
    }
}
`;

export const CardHeader = styled.div`
    display: flex;
    padding: 16px;
    align-items: center;
`;

export const CircleAvatar = styled.div`
    width: 40px;
    height: 40px;
    display: flex;
    overflow: hidden;
    position: relative;
    font-size: ${props => props.theme.font.large};
    align-items: center;
    flex-shrink: 0;
    font-family: ${props => props.theme.font.fontFamily};
    line-height: 1;
    user-select: none;
    border-radius: 50%;
    justify-content: center;
    `

export const CardTitle = styled.div`
    overflow: hidden;
    white-space: nowrap;
    padding-left: 12px;
    padding-right: 12px;
    text-overflow: ellipsis;
`;

export const CardDescription = styled.p`
    margin-top: 10px;
    white-space: normal;
    font-size: ${props => props.theme.paragraph.fontSize};
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    font-weight: ${props => props.theme.paragraph.fontWeight};
    color: ${props => props.theme.paragraph.color};
    padding: 10px;
    text-align: center;

    @media screen and (max-width: ${MediaBreakpoints.extraLargeDevice}) {
        font-size: ${props => props.theme.font.small};
    }
    
    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        display: none;
    } 
`;

export const CardImage = styled.div`
    display: block;
    height: 60%;    
    transition: height .5s;

    @media screen and (max-width: ${MediaBreakpoints.extraLargeDevice}) {
        height: 40%;
    }    

    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        height: 60%;
    } 

    &:hover {
        height: 100%;
    }
`;
