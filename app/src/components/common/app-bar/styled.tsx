import styled from 'styled-components';
import { MediaBreakpoints } from '../../../utils/styles/media-breakpoints';

export const NavBar = styled.div`
    top: 0;
    left: 0;
    height: 70px;
    width: 100%;
    background-color: ${(props) => props.theme.colors.white};
    box-shadow: ${(props) => props.theme.boxShadows.primary};
    margin-bottom: 3rem;
    display: flex;
    justify-content: flex-end;
    transition: justify-content .5s;
    
    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        justify-content: center;
    }
`;