import React from 'react';
import { NavBar } from './styled';
import Button from '../button';
import { Link } from 'react-router-dom';

const AppBar = () => {
    const noop = () => { };
    return (
        <NavBar>
            <Link to="/"><Button text="HOME" onClick={noop} /></Link>
        </NavBar>
    );
}

export default AppBar;