import styled from 'styled-components';

export const Buttons = styled.div`
    display: flex;
    justify-content: flex-end;
    margin: 1rem 7rem;
`