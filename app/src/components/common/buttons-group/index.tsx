import React, { useState } from 'react';
import Button from '../button';
import { Buttons } from './styled';

interface IButtonProps {
    id: string;
    onClick: () => void;
    text: string;
    disabled?: boolean;
}

interface IProps {
    buttons: IButtonProps[];
}

const ButtonsGroup = ({ buttons }: IProps) => {
    const [active, setActive] = useState('');

    const onClick = (id: string, callback: () => void) => {
        setActive(id);
        callback();
    }

    return (
        <Buttons>
            {
                buttons.map(button =>
                    <Button
                        active={active === button.id}
                        key={button.id}
                        text={button.text}
                        onClick={() => onClick(button.id, button.onClick)}
                    />)
            }
        </Buttons>
    )
}

export default ButtonsGroup;