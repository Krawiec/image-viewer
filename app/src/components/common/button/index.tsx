import * as React from 'react';
import { Button as ButtonStyled } from './styled';

export type Props = {
    onClick: () => void;
    text: string;
    disabled?: boolean;
    active?: boolean
};

const Button = ({ onClick, text, active = false, disabled = false }: Props) => {
    return (
        <ButtonStyled active={active} disabled={disabled} onClick={onClick}>
            {text}
        </ButtonStyled>
    );
};

export default Button;