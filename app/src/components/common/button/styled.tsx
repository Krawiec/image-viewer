import styled from 'styled-components';

interface IButtonProps {
    disabled: boolean;
    active: boolean;
}

export const Button = styled.button<IButtonProps>`
    min-height: 50px;
    width: 300px;
    background: ${(props) =>
    (props.disabled
        ? props.theme.colors.disable
        : props.active ? props.theme.button.active : props.theme.colors.white)};
    color: ${props => props.theme.colors.black};
    display: flex;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
    border: none;
    font-size: ${props => props.theme.font.medium};
    font-weight: 700;
    transition: all 0.25s ease-in-out;
    box-shadow: ${(props) => props.theme.button.boxShadow};
    margin: 10px;
    
    &:hover {
        cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
        background: ${(props) => !props.disabled && props.theme.colors.hover};
    };
`;
