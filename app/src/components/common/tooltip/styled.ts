import styled from 'styled-components';

export const TooltipContainer = styled.div`
    position: relative;
`;

export const StyledTooltip = styled.div`
    position: absolute;
    left: 50%;
    width: 200px;
    height: 50px;
    border: 1px solid black;
    border-radius: 5px;
    transform: translate(-50%, 20%);
    display: flex;
    justify-content: center;
    align-items: center;
    background: white;
    z-index: 1000;
`;
