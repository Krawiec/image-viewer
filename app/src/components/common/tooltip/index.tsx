import React, { useCallback, useState } from 'react';
import { TooltipContainer, StyledTooltip } from './styled';

interface IProps {
    text: string;
    children: React.ReactElement;
}

const Tooltip = ({ text, children }: IProps) => {
    const [show, setShow] = useState(false);

    const showTooltip = useCallback(() => setShow(true), []);
    const hideTooltip = useCallback(() => setShow(false), []);

    return (
        <TooltipContainer
            onMouseOver={showTooltip}
            onMouseOut={hideTooltip}
        >
            {show && <StyledTooltip>{text}</StyledTooltip>}
            {children}
        </TooltipContainer>
    )
}

export default Tooltip;