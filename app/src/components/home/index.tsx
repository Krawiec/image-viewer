import React from 'react';
import Topics from './topics';
import LatestImages from './latest-images';

export default () => {
    return (
        <>
            <Topics />
            <LatestImages />
        </>
    );
}