import styled, { keyframes } from 'styled-components';
import { MediaBreakpoints } from '../../../utils/styles/media-breakpoints';

export const ImagesWrapper = styled.div`
    display: flex;
    flex-wrap: wrap; 
    align-items: center;
    justify-content: center;
`;

const bounce = keyframes`
    0%   { transform: translateY(0); }
    50%  { transform: translateY(-10px); }
    100% { transform: translateY(0); }
`;

export const ImageWrapper = styled.div`
    height: 400px;
    width: 400px;
    margin: 10px 15px;  
    border-radius: 10%;  
    overflow: hidden;
    animation-duration: 0.5s;
    transition: height .5s, width .5s;    
    border: 1px solid ${props => props.theme.colors.shadowed};

    &:hover {
        animation-name: ${bounce};
        animation-timing-function: infinite ease;
    }
    
    @media screen and (max-width: ${MediaBreakpoints.extraLargeDevice}) {
        height: 300px;
        width: 300px;
    }    

    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        height: 200px;
        width: 200px;
    } 
`;