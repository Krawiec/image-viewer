import React from 'react';
import { Basic } from 'unsplash-js/dist/methods/photos/types';
import { useFetch } from '../../../hooks/use-fetch';
import { PhotoPreview } from '../../../types/photo';
import { getLatestTenPhotos } from '../../../utils/unsplash';
import { Header } from '../../common/styled/header.styled';
import Loader from '../../common/loader';
import PhotosList from '../../common/image/list';
import { ApiResponse } from 'unsplash-js/dist/helpers/response';

const filterResponse = (response: ApiResponse<{ results: Basic[], total: number }>) => {
    return response.response!.results.map(item => ({
        id: item.id,
        urls: item.urls,
        likes: item.likes,
        width: item.width,
        height:
            item.height
    } as PhotoPreview));
}

const LatestImages = () => {
    const { isLoading, response } = useFetch(
        getLatestTenPhotos,
        filterResponse);
    return (
        <>
            <Header>Latest and most popular</Header>
            <Loader loading={isLoading}>
                {
                    response && (
                        <PhotosList photos={response!} />
                    )
                }
            </Loader>
        </>
    )
}

export default LatestImages;