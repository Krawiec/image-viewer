import React from 'react';
import { shallow } from 'enzyme';
import * as useFetch from '../../../hooks/use-fetch';
import LatestImages from './index';
import Loader from '../../common/loader';
import PhotosList from '../../common/image/list';
import { photos } from '../../../mocks/photos';

afterEach(() => {
    jest.clearAllMocks();
});

jest.mock('../../../hooks/use-fetch');

describe('<LatestImages />', () => {
    const useFetchMock = useFetch as jest.Mocked<
        typeof useFetch
    >;

    const mockUseFetchingResponse = (isLoading: boolean, response: any) =>
        useFetchMock.useFetch.mockReturnValue({
            isLoading,
            response,
            error: null
        });

    test('Should show loader when fetching data', () => {
        mockUseFetchingResponse(true, null);
        const wrapper = shallow(<LatestImages />);
        const loader = wrapper.find(Loader);
        const list = wrapper.find(PhotosList);

        expect(loader.prop('loading')).toBeTruthy();
        expect(list.length).toEqual(0);
    });

    test('Should show loader when fetching data', () => {
        mockUseFetchingResponse(false, photos);
        const wrapper = shallow(<LatestImages />);
        const loader = wrapper.find(Loader);
        const list = wrapper.find(PhotosList);

        expect(loader.prop('loading')).toBeFalsy();
        expect(list.props()).toEqual(({
            photos
        }));
    });
})