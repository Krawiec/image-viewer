import React from 'react';
import { ITopic } from '../../../types/topic';
import { TopicsContainer } from './styled';
import TopicItemCard from './topic-item';

export interface IProps {
    topics: ITopic[];
}

const TopicList = ({ topics }: IProps) => {
    return topics.length > 0 ? (
        <TopicsContainer>
            {topics.map(
                topic =>
                    <TopicItemCard key={topic.id} topic={topic} />)}
        </TopicsContainer>
    ) : (<></>)
}

export default TopicList;