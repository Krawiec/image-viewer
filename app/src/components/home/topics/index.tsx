import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTopics } from '../../../redux/topics/action-creators';
import { getTopics, loadingTopics } from '../../../redux/topics/selectors';
import TopicsList from './list';
import Loader from '../../common/loader';
import { Header } from '../../common/styled/header.styled';

const Topic = () => {
    const dispatch = useDispatch();
    const topics = useSelector(getTopics);
    const loading = useSelector(loadingTopics);

    useEffect(() => {
        if (topics.length === 0) {
            dispatch(fetchTopics());
        }
    }, [topics]);

    return (
        <>
            <Header>Select topic</Header>
            <Loader loading={loading}>
                <TopicsList topics={topics} />
            </Loader>
        </>
    )
}

export default Topic;