import React from 'react';
import { shallow } from 'enzyme';
import Topics from './index';
import * as topicsSelectors from '../../../redux/topics/selectors';
import { IReduxState } from '../../../redux/types';
import { ITopic } from '../../../types/topic';
import TopicsList from './list';
import Loader from '../../common/loader';

afterEach(() => {
    jest.clearAllMocks();
});

jest.mock('react-redux', () => ({
    useSelector: jest.fn((fn) => fn()),
    useDispatch: jest.fn()
}));
jest.mock('../../../redux/topics/selectors');

const topic: ITopic = {
    id: 'adas',
    cover_photo: {
        urls: {
            full: '',
            raw: '',
            regular: '',
            small: '',
            thumb: ''
        }
    },
    description: 'adas',
    title: 'adsa',
    slug: ''
};

const createTopics = (numOfItems: number): ITopic[] => Array(numOfItems).fill(topic).map(n => ({ ...topic, id: n.toString() } as ITopic))

describe('<Topics /> tests', () => {
    const render = () => shallow(<Topics />);
    const selectorsMocks = topicsSelectors as jest.Mocked<
        typeof topicsSelectors
    >;
    const loadingTopicSelectorMock = (loading: boolean) => selectorsMocks.loadingTopics.mockImplementationOnce(
        (_: IReduxState) => loading,
    );

    const loadingGetTopicsSelectorMock = (topics: ITopic[]) =>
        selectorsMocks.getTopics.mockImplementationOnce(
            (_: IReduxState) => topics,
        );

    it.each`
        loading  | topics | exists
        ${true}  | ${[]}  | ${true}
        ${false} | ${[]}  | ${false}
    `('<Topics/> renders Loader depends on selector state', ({ loading, topics, exists }) => {
        loadingTopicSelectorMock(loading);
        loadingGetTopicsSelectorMock(topics);
        const sut = render();

        const loader = sut.find(Loader);
        expect(loader.prop('loading')).toBe(exists);
    });

    it.each`
        loading  | topics             | expectedNumberOfTopics
        ${false} | ${createTopics(5)} | ${5}
        ${false} | ${createTopics(0)} | ${0}
    `('<Topics/> renders expected number of topics depends on selector state', ({ loading, topics, expectedNumberOfTopics }) => {
        loadingTopicSelectorMock(loading);
        loadingGetTopicsSelectorMock(topics);
        const sut = render();

        const topicLists = sut.find(TopicsList);
        expect(topicLists.prop('topics').length).toBe(expectedNumberOfTopics);
    });
})