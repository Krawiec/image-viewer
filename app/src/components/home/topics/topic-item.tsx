import React from 'react';
import Card from '../../common/card';
import { Link } from 'react-router-dom';
import { ITopic } from '../../../types/topic';
import RoutePaths from '../../../utils/routing';

export interface IProps {
    topic: ITopic;
}

const TopicItemCard = ({ topic }: IProps) => {
    return (
        <Link
            to={RoutePaths.TOPIC.formatPath(topic.slug)!}
            key={topic.id}
        >
            <Card
                title={topic.title}
                description={topic.description}
                imgSrc={topic.cover_photo.urls.small}
            />
        </Link>
    )
}

export default TopicItemCard;