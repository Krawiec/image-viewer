import React from 'react';
import { shallow } from 'enzyme';
import TopicItemCard, { IProps } from './topic-item';
import { ITopic } from '../../../types/topic';
import { Link } from 'react-router-dom';
import Card from '../../common/card';

const defaultTopic: ITopic = {
    cover_photo: {
        urls: {
            full: 'full',
            raw: 'raw',
            regular: 'regular',
            small: 'small',
            thumb: 'thumb'
        }
    },
    description: 'description',
    id: 'id',
    slug: 'slug',
    title: 'title'
};

const getProps = (topic: ITopic = defaultTopic): IProps => ({
    topic
});

const render = (props: IProps) => shallow(<TopicItemCard {...props} />);

describe('<TopicItemCard /> tests', () => {
    test('Link component should have exected route', () => {
        const props = getProps();
        const wrapper = render(props);
        const sut = wrapper.find(Link);

        expect(sut.prop('to')).toBe(`/topic/${props.topic.slug}`);
    });

    test('Card component should containes expected props', () => {
        const props = getProps();
        const wrapper = render(props);
        const sut = wrapper.find(Card);

        const {
            title,
            description,
            cover_photo: {
                urls: {
                    small
                }
            }
        } = props.topic;

        expect(sut.props()).toEqual({
            title,
            description,
            imgSrc: small
        })
    });
})