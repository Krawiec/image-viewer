import styled from 'styled-components';

export const TopicsContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items:center;
    flex-wrap: wrap;
    margin-bottom: 20px;
`;