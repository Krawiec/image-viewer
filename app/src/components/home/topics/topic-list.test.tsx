import React from 'react';
import { shallow } from 'enzyme';
import TopicList, { IProps } from './list';
import { ITopic } from '../../../types/topic';
import topicsMock from '../../../mocks/topics';
import TopicItemCard from './topic-item';

const getProps = (topics: ITopic[] = topicsMock): IProps => ({
    topics
});

const render = (props: IProps) => shallow(<TopicList {...props} />);
describe('<TopicList /> component', () => {
    test('Should render expected number of items', () => {
        const props = getProps();
        const wrapper = render(props);

        const sut = wrapper.find(TopicItemCard);

        expect(sut.length).toBe(topicsMock.length);
    })
})