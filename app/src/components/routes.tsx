import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import RoutePaths from '../utils/routing';
import Layout from './common/layout';

const Home = React.lazy(() => import('./home'));
const Topic = React.lazy(() => import('./topic'));
const Photo = React.lazy(() => import('./photo'));

const Routes = () => {
    return (
        <React.Suspense fallback={<div>Loading...</div>}>
            <Router>
                <Switch>
                    <Layout>
                        <Route exact component={Home} path={RoutePaths.HOME} />
                        <Route exact component={Topic} path={RoutePaths.TOPIC.path} />
                        <Route exact component={Photo} path={RoutePaths.PHOTO.path} />
                    </Layout>
                </Switch>
            </Router>
        </React.Suspense>
    )
};

export default Routes;