import React, { useCallback } from 'react'
import { useParams } from 'react-router';
import { ApiResponse } from 'unsplash-js/dist/helpers/response';
import { Full } from 'unsplash-js/dist/methods/photos/types';
import { useFetch } from '../../hooks/use-fetch';
import { IPhoto } from '../../types/photo';
import { getPhoto } from '../../utils/unsplash';
import Loader from '../common/loader';
import FullPhoto from './full-photo';

const filterResponse = (response: ApiResponse<Full>) => {
    const item = response.response!;
    return {
        id: item.id,
        urls: item.urls,
        likes: item.likes,
        width: item.width,
        height:
            item.height,
        location: item.location,
        description: item.description,
        alt_description: item.alt_description,
        user: {
            location: item.user.location,
            name: item.user.name,
            username: item.user.username,
            portfolio: item.user.portfolio_url
        }
    } as IPhoto;
}

const Photo = () => {
    const { photoId } = useParams<{ photoId: string }>();

    const fetchPhoto = useCallback(() => getPhoto(photoId), []);

    const { isLoading, response } = useFetch(
        fetchPhoto,
        filterResponse);

    return (
        <Loader loading={isLoading}>
            <FullPhoto photo={response!} />
        </Loader>
    )
}

export default Photo;