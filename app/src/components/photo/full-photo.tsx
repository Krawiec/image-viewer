import React from 'react';
import { IPhoto } from '../../types/photo';
import { Header } from '../common/styled/header.styled';
import { Container, PhotoWrapper } from './styled';
import Image from '../common/image'

interface IProps {
    photo: IPhoto
}

const FullPhoto = ({ photo }: IProps) => {
    return photo ? (
        <>
            <Header>{photo?.description ?? photo!.alt_description ?? 'Lack of description'}</Header>
            <Container>
                <PhotoWrapper>
                    <Image src={photo!.urls.regular} />
                </PhotoWrapper>
                <div>
                    <div data-att="name">{photo.user.name}</div>
                    <div data-att="username">{photo.user.username}</div>
                    <div data-att="location">{photo.user.location}</div>
                    <div data-att="portfolio">{photo.user.portfolio}</div>
                </div>
            </Container>
        </>
    ) : <></>
}

export default FullPhoto;