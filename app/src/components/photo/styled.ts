import styled from 'styled-components';
import { MediaBreakpoints } from '../../utils/styles/media-breakpoints';

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

export const PhotoWrapper = styled.div`
    width: 800px;
    height: 700px;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: height 0.5s, width 0.5s;

    @media screen and (max-width: ${MediaBreakpoints.extraLargeDevice}) {
        width: 450px;
        height: 450px;
    }

    @media screen and (max-width: ${MediaBreakpoints.extraSmallDevice}) {
        height: 350px;
        width: 450px;
    }
`;
