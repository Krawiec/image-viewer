import React from 'react';
import { shallow } from 'enzyme';
import Topic from './index';
import * as selectors from '../../redux/topic-photos/selectors';
import { IReduxState } from '../../redux/types';
import { PhotoPreview } from '../../types/photo';
import Loader from '../common/loader';
import ImagesList from '../common/image/list';
import { photos } from '../../mocks/photos';

afterEach(() => {
    jest.clearAllMocks();
});

jest.mock('react-redux', () => ({
    useSelector: jest.fn((fn) => fn()),
    useDispatch: jest.fn()
}));

jest.mock('react-router-dom', () => ({
    useParams: jest.fn(() => ({ topicId: 'test' }))
}))

jest.mock('../../hooks/use-infite-scroll', () => jest.fn());
jest.mock('../../redux/topic-photos/selectors');

describe('<Topic />', () => {
    const selectorsMocks = selectors as jest.Mocked<
        typeof selectors
    >;
    const fetchingTopicPhotosMock = (loading: boolean) => selectorsMocks.fetchingTopicPhotos.mockImplementationOnce(
        (_: IReduxState) => loading,
    );
    const getTopicPhotosMock = (photos: PhotoPreview[]) => selectorsMocks.getTopicPhotos.mockImplementationOnce(
        (_: IReduxState) => photos,
    );

    test('Should display Loader when fetching topics', () => {
        fetchingTopicPhotosMock(true);
        getTopicPhotosMock([]);
        const wrapper = shallow(<Topic />);
        const loader = wrapper.find(Loader);
        const list = wrapper.find(ImagesList);

        expect(loader.prop('loading')).toBeTruthy();
        expect(list.prop('photos')).toEqual([]);
    });

    test('Should pass topic photo to list component', () => {
        fetchingTopicPhotosMock(false);
        getTopicPhotosMock(photos);
        const wrapper = shallow(<Topic />);
        const loader = wrapper.find(Loader);
        const list = wrapper.find(ImagesList);

        expect(loader.prop('loading')).toBeFalsy();
        expect(list.prop('photos')).toEqual(photos);
    })
})
