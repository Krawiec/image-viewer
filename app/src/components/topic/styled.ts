import styled from 'styled-components';

export const LoaderWrapper = styled.div`
    width: 100%;
    height: 200px;
`;
