import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { fetchPhotos, clearResults } from '../../redux/topic-photos/action-creators';
import { getTopicPhotos, fetchingTopicPhotos } from '../../redux/topic-photos/selectors';
import Loader from '../common/loader';
import ImagesList from '../common/image/list';
import { Header } from '../common/styled/header.styled';
import { LoaderWrapper } from './styled';
import useInfiniteScroll from '../../hooks/use-infite-scroll';
import { OrderBy } from 'unsplash-js';
import OrderButtons from './order-buttons';

const Topic = () => {
    const dispatch = useDispatch();
    const { topicId } = useParams<{ topicId: string }>();
    const topicPhotos = useSelector(getTopicPhotos);
    const isLoading = useSelector(fetchingTopicPhotos);

    const [page, setPage] = useState(0);
    const getNextPage = () => {
        setPage(curr => curr + 1);
    }

    const [order, setOrder] = useState(OrderBy.LATEST);
    const orderChange = (order: OrderBy) => setOrder(order);

    const loaderReference = useRef<HTMLImageElement>(null);

    useEffect(() => {
        dispatch(clearResults());
        setPage(0);
    }, [topicId, order]);

    useEffect(() => {
        dispatch(fetchPhotos(topicId, page, order));
    }, [page]);


    useInfiniteScroll({
        isLoading,
        target: loaderReference,
        loadMore: getNextPage
    });

    return (
        <>
            <Header>{topicId.toUpperCase()}</Header>
            <OrderButtons changeOrder={orderChange} />
            <ImagesList photos={topicPhotos!} />
            <LoaderWrapper ref={loaderReference}><Loader loading={isLoading} /></LoaderWrapper>
        </>
    )
}

export default React.memo(Topic);