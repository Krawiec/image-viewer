import React, { useMemo } from 'react';
import { OrderBy } from 'unsplash-js';
import ButtonsGroup from '../common/buttons-group';

interface IProps {
    changeOrder: (order: OrderBy) => void;
}

const OrderButtons = ({ changeOrder }: IProps) => {
    const buttons = useMemo(() => [
        {
            id: 'latest',
            text: 'LATEST',
            onClick: () => changeOrder(OrderBy.LATEST)
        },
        {
            id: 'popular',
            text: 'POPULAR',
            onClick: () => changeOrder(OrderBy.POPULAR)
        }
    ], []);

    return (
        <ButtonsGroup buttons={buttons} />
    )
}

export default OrderButtons;