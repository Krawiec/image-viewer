import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { store, history } from '../redux/configure-store';
import theme from '../utils/theme';
import ErrorBoundary from './error-boundary';
import Routes from './routes';

const App = () => {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <ErrorBoundary>
                    <ThemeProvider theme={theme}>
                        <Routes />
                    </ThemeProvider>
                </ErrorBoundary>
            </ConnectedRouter>
        </Provider>
    )
}

export default App;