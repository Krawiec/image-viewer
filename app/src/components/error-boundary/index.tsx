import React, { ErrorInfo, ReactNode } from 'react';

interface IState {
    hasError: boolean;
    error?: Error
}

interface IProps {
    children: ReactNode;
}

class ErrorBoundary extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    componentDidCatch(error: Error, _: ErrorInfo) {
        this.setState({
            hasError: true,
            error
        });
    }

    render() {
        const { hasError, error } = this.state;
        if (hasError) {
            return (
                <div>Error occured: {error!.message}!</div>
            )
        }

        return this.props.children;
    }
}

export default ErrorBoundary;