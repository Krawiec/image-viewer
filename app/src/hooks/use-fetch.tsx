import { useEffect, useState } from 'react'
import { ApiResponse } from 'unsplash-js/dist/helpers/response';

export const useFetch = <O extends unknown>(
    fetchFn: (signal?: AbortSignal) => Promise<ApiResponse<any>>,
    filter: (item: ApiResponse<any>) => O) => {
    const [response, setResponse] = useState<O>();
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<Error | null>(null);

    useEffect(() => {
        const abortController = new AbortController();
        const { signal } = abortController;
        const fetch = async () => {
            setIsLoading(true);
            try {
                const apiReponse = await fetchFn(signal);
                setResponse(filter(apiReponse));
            } catch (e) {
                setError(e);
            } finally {
                setIsLoading(false);
            }
        };
        fetch();

        return () => {
            abortController.abort();
        }
    }, [fetchFn]);

    return {
        response,
        isLoading,
        error
    }
}