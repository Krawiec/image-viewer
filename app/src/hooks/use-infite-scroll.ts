import { useCallback, useLayoutEffect, useRef } from 'react';
import useObserver from './use-observer';

interface IProps {
    isLoading: boolean;
    target: React.RefObject<HTMLElement>;
    loadMore: () => void;
}

const useInfiniteScroll = ({ isLoading, loadMore, target }: IProps) => {
    const isScrolling = useRef(false);

    useLayoutEffect(() => {
        let timeout: NodeJS.Timeout | null;

        const handleScroll = () => {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(() => {
                timeout = null;
                isScrolling.current = false;
            }, 1000);

            if (!isScrolling.current) {
                isScrolling.current = true;
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    const load = useCallback(
        (
            [{ isIntersecting }]: IntersectionObserverEntry[],
            _: IntersectionObserver,
        ): void => {
            if (isIntersecting && isScrolling.current) {
                loadMore();
            }
        },
        [isLoading],
    );

    useObserver({
        target: target!,
        onIntersect: load,
        threshold: 1.0,
        rootMargin: '0px 0px 0px 0px',
    });
};

export default useInfiniteScroll;
