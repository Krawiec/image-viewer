import { useEffect } from 'react';

interface IObserverProps {
    target: React.RefObject<HTMLElement>;
    onIntersect: (
        entries: IntersectionObserverEntry[],
        observer: IntersectionObserver,
    ) => void;
    threshold?: number;
    rootMargin?: string;
}

const useObserver = ({
    target,
    onIntersect,
    threshold = 0,
    rootMargin = '0px',
}: IObserverProps) => {
    useEffect(() => {
        const observer = new IntersectionObserver(onIntersect, {
            rootMargin,
            threshold,
            root: null,
        });

        const { current } = target;

        observer.observe(current!);

        () => {
            observer.unobserve(current!);
        };
    }, []);
};

export default useObserver;
