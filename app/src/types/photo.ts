import { IUrl } from './url';

interface IUser {
    username: string;
    name: string;
    portfolio: string;
    location: string;
}

interface IPhoto {
    id: string;
    downloads?: number;
    likes: number;
    location: {
        city: string;
        country: string;
    };
    urls: IUrl;
    width: number;
    height: number;
    description?: string;
    alt_description: string;
    user: IUser;
}

type PhotoPreview = Pick<IPhoto, 'id' | 'urls' | 'likes' | 'width' | 'height'>;

export { PhotoPreview, IPhoto };
