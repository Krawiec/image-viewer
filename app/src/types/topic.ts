import { IUrl } from './url';

export interface ITopic {
    id: string;
    slug: string;
    title: string;
    description: string;
    cover_photo: {
        urls: IUrl;
    };
}
