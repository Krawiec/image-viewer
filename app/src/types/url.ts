export interface IUrl {
    raw: string;
    full: string;
    regular: string;
    small: string;
    thumb: string;
}
