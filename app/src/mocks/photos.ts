import { PhotoPreview } from '../types/photo';

export const photos: PhotoPreview[] = [
    {
        id: 'K2LEKoEGL0s',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1611095969382-19fbe23cebff?ixid=MnwyMjg2MDN8MXwxfGFsbHwxfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1611095969382-19fbe23cebff?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MXwxfGFsbHwxfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1611095969382-19fbe23cebff?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MXwxfGFsbHwxfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1611095969382-19fbe23cebff?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MXwxfGFsbHwxfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1611095969382-19fbe23cebff?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MXwxfGFsbHwxfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 220,
    },
    {
        id: 'YNtXfFJ0ejo',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620307264118-70a5ffb5cb30?ixid=MnwyMjg2MDN8MHwxfGFsbHwyfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620307264118-70a5ffb5cb30?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwyfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620307264118-70a5ffb5cb30?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwyfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620307264118-70a5ffb5cb30?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwyfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620307264118-70a5ffb5cb30?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwyfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 2,
    },
    {
        id: 'rUTpMpG6GEQ',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620056389861-6caeeb699ed2?ixid=MnwyMjg2MDN8MHwxfGFsbHwzfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620056389861-6caeeb699ed2?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwzfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620056389861-6caeeb699ed2?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwzfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620056389861-6caeeb699ed2?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwzfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620056389861-6caeeb699ed2?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwzfHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 5,
    },
    {
        id: 'I7iVmvtRouE',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620195408077-817b53ce0473?ixid=MnwyMjg2MDN8MHwxfGFsbHw0fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620195408077-817b53ce0473?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw0fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620195408077-817b53ce0473?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw0fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620195408077-817b53ce0473?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw0fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620195408077-817b53ce0473?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw0fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 4,
    },
    {
        id: 'K0GYFcAVhj8',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620246499650-86ee085562fa?ixid=MnwyMjg2MDN8MHwxfGFsbHw1fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620246499650-86ee085562fa?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw1fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620246499650-86ee085562fa?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw1fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620246499650-86ee085562fa?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw1fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620246499650-86ee085562fa?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw1fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 7,
    },
    {
        id: 'zTsdJ2dQjDU',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620291044431-4852febd2022?ixid=MnwyMjg2MDN8MHwxfGFsbHw2fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620291044431-4852febd2022?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw2fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620291044431-4852febd2022?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw2fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620291044431-4852febd2022?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw2fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620291044431-4852febd2022?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw2fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 25,
    },
    {
        id: 'Ks4qEshj_uI',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620265796157-1cb8978639b8?ixid=MnwyMjg2MDN8MHwxfGFsbHw3fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620265796157-1cb8978639b8?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw3fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620265796157-1cb8978639b8?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw3fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620265796157-1cb8978639b8?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw3fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620265796157-1cb8978639b8?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw3fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 21,
    },
    {
        id: 'aWYwE9QReIE',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620281487148-401d1fa8f907?ixid=MnwyMjg2MDN8MHwxfGFsbHw4fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620281487148-401d1fa8f907?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw4fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620281487148-401d1fa8f907?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw4fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620281487148-401d1fa8f907?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw4fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620281487148-401d1fa8f907?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw4fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 5,
    },
    {
        id: 'licYp8spO1A',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620057657132-e737f4983bc6?ixid=MnwyMjg2MDN8MHwxfGFsbHw5fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620057657132-e737f4983bc6?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw5fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620057657132-e737f4983bc6?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw5fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620057657132-e737f4983bc6?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw5fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620057657132-e737f4983bc6?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHw5fHx8fHx8Mnx8MTYyMDMyODMwMg&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 12,
    },
    {
        id: '0bDCoo6yhqc',
        width: 8063,
        height: 5375,
        urls: {
            raw:
                'https://images.unsplash.com/photo-1620283015216-958ef6600e05?ixid=MnwyMjg2MDN8MHwxfGFsbHwxMHx8fHx8fDJ8fDE2MjAzMjgzMDI&ixlib=rb-1.2.1',
            full:
                'https://images.unsplash.com/photo-1620283015216-958ef6600e05?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwxMHx8fHx8fDJ8fDE2MjAzMjgzMDI&ixlib=rb-1.2.1&q=85',
            regular:
                'https://images.unsplash.com/photo-1620283015216-958ef6600e05?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwxMHx8fHx8fDJ8fDE2MjAzMjgzMDI&ixlib=rb-1.2.1&q=80&w=1080',
            small:
                'https://images.unsplash.com/photo-1620283015216-958ef6600e05?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwxMHx8fHx8fDJ8fDE2MjAzMjgzMDI&ixlib=rb-1.2.1&q=80&w=400',
            thumb:
                'https://images.unsplash.com/photo-1620283015216-958ef6600e05?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjg2MDN8MHwxfGFsbHwxMHx8fHx8fDJ8fDE2MjAzMjgzMDI&ixlib=rb-1.2.1&q=80&w=200',
        },
        likes: 14,
    },
];
