import { ITopic } from '../types/topic';

const topicsMock: ITopic[] = [
    {
        id: 'bo8jQKTaE0Y',
        slug: 'wallpapers',
        title: 'Wallpapers',
        description:
            'From epic drone shots to inspiring moments in nature, find free HD wallpapers worthy of your mobile and desktop screens. Finally.',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/12/gladiator.jpg?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/12/gladiator.jpg?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/12/gladiator.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/12/gladiator.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/12/gladiator.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: '6sMVjTLSkeQ',
        slug: 'nature',
        title: 'Nature',
        description:
            'Let’s celebrate the magic of Mother Earth — with images of everything our planet has to offer, from stunning seascapes, starry skies, and everything in between.',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/41/LGhxuAbT5Wop4JYcrMpV_IMG_3808.jpg?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/41/LGhxuAbT5Wop4JYcrMpV_IMG_3808.jpg?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/41/LGhxuAbT5Wop4JYcrMpV_IMG_3808.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/41/LGhxuAbT5Wop4JYcrMpV_IMG_3808.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/41/LGhxuAbT5Wop4JYcrMpV_IMG_3808.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'towJZFskpGg',
        slug: 'people',
        title: 'People',
        description:
            'Real people, captured. Photography has the power to reflect the world around us, give voice to individuals and groups within our communities — and most importantly — tell their story. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/photo-1421986583638-7966e436a03a?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/photo-1421986583638-7966e436a03a?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/photo-1421986583638-7966e436a03a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/photo-1421986583638-7966e436a03a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/photo-1421986583638-7966e436a03a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'rnSKDHwwYUk',
        slug: 'architecture',
        title: 'Architecture',
        description:
            'Explore exteriors from around the world — from brutalist buildings to minimalist structures that will give you a new appreciation for the art of architecture. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/reserve/nTr1589kTgyXCOdStCGm_MikaRuusunen.jpg?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/reserve/nTr1589kTgyXCOdStCGm_MikaRuusunen.jpg?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/reserve/nTr1589kTgyXCOdStCGm_MikaRuusunen.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/reserve/nTr1589kTgyXCOdStCGm_MikaRuusunen.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/reserve/nTr1589kTgyXCOdStCGm_MikaRuusunen.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'BJJMtteDJA4',
        slug: 'current-events',
        title: 'Current Events',
        description:
            'Covering the latest important events and news-worthy moments from around the world — including climate protests, pride parades, holiday celebrations, and more. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/photo-1437913135140-944c1ee62782?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/photo-1437913135140-944c1ee62782?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/photo-1437913135140-944c1ee62782?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/photo-1437913135140-944c1ee62782?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/photo-1437913135140-944c1ee62782?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'aeu6rL-j6ew',
        slug: 'business-work',
        title: 'Business & Work',
        description:
            'Reflecting the realities of the modern workplace in their many forms — from images of remote working and start-ups to shots of engineers and artists at work.',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/1/bag-and-hands.jpg?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/1/bag-and-hands.jpg?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/1/bag-and-hands.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/1/bag-and-hands.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/1/bag-and-hands.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'qPYsDzvJOYc',
        slug: 'experimental',
        title: 'Experimental',
        description:
            'Through the use of unlikely textures, intriguing subject matter, and new formats — photography has the power to challenge our perspectives and push creativity forward. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/photo-1429637119272-20043840c013?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/photo-1429637119272-20043840c013?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/photo-1429637119272-20043840c013?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/photo-1429637119272-20043840c013?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/photo-1429637119272-20043840c013?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'S4MKLAsBB74',
        slug: 'fashion',
        title: 'Fashion',
        description:
            'From street-style shots to editorial photography — find the latest trends in beauty and fashion. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/photo-1434056886845-dac89ffe9b56?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/photo-1434056886845-dac89ffe9b56?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/photo-1434056886845-dac89ffe9b56?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/photo-1434056886845-dac89ffe9b56?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/photo-1434056886845-dac89ffe9b56?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: 'hmenvQhUmxM',
        slug: 'film',
        title: 'Film',
        description:
            'Beautiful analog photography from the past and present day — featuring polaroids, black &amp; white images, grainy portraits, and more.',
        cover_photo: {
            urls: {
                raw: 'https://images.unsplash.com/19/nomad.JPG?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/19/nomad.JPG?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/19/nomad.JPG?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/19/nomad.JPG?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/19/nomad.JPG?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
    {
        id: '_hb-dl4Q-4U',
        slug: 'health',
        title: 'Health & Wellness',
        description:
            'Celebrate a healthy mind, body and soul with photographs that showcase everything from new medical discoveries and alternative medicines, to healthy eating and meditation. ',
        cover_photo: {
            urls: {
                raw:
                    'https://images.unsplash.com/31/RpgvvtYAQeqAIs1knERU_vegetables.jpg?ixlib=rb-1.2.1',
                full:
                    'https://images.unsplash.com/31/RpgvvtYAQeqAIs1knERU_vegetables.jpg?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb',
                regular:
                    'https://images.unsplash.com/31/RpgvvtYAQeqAIs1knERU_vegetables.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max',
                small:
                    'https://images.unsplash.com/31/RpgvvtYAQeqAIs1knERU_vegetables.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max',
                thumb:
                    'https://images.unsplash.com/31/RpgvvtYAQeqAIs1knERU_vegetables.jpg?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max',
            },
        },
    },
];

export default topicsMock;
