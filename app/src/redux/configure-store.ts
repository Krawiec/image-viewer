import { createBrowserHistory } from 'history';
import Redux, { applyMiddleware, compose, createStore } from 'redux';

import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { IReduxState } from './types';
import combinedRootReducer from './root';

const history = createBrowserHistory();

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const configureStore = () => {
    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
        process.env.NODE_ENV !== 'production'
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            : compose;

    const store: Redux.Store<IReduxState> = createStore(
        combinedRootReducer(history),
        composeEnhancers(applyMiddleware(thunk, routerMiddleware(history))),
    );

    return store;
};

const store = configureStore();

export { store, configureStore, history };
