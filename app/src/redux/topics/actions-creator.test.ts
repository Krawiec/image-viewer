import * as getTopics from '../../utils/unsplash';
import { ApiResponse } from 'unsplash-js/dist/helpers/response';
import { Basic } from 'unsplash-js/dist/methods/topics/types';
import { fetchTopics } from './action-creators';
import * as actions from './action-creators';

afterEach(() => {
    jest.clearAllMocks();
});

jest.mock('../../utils/unsplash');

const basicResponse = {
    id: '1',
    cover_photo: null,
    description: '',
    slug: 'slug',
    title: 'title',
} as Basic;

const response: ApiResponse<{
    results: Basic[];
    total: number;
}> = {
    originalResponse: {} as Response,
    response: {
        results: [basicResponse],
        total: 0,
    },
    status: 200,
    type: 'success',
    errors: undefined,
};

describe('fetchTopics action', () => {
    const getTopicsMock = getTopics as jest.Mocked<typeof getTopics>;
    const getTopicsMockResponseSuccess = () =>
        getTopicsMock.getTopics.mockImplementationOnce(() =>
            Promise.resolve(response),
        );
    const getTopicsMockResponseError = () =>
        getTopicsMock.getTopics.mockImplementationOnce(() => Promise.reject());

    const fetchTopicsStartMock = jest.spyOn(actions, 'fetchTopicsStart');
    const fetchTopicsSuccessMock = jest.spyOn(actions, 'fetchTopicsSuccess');
    const fetchTopicsFailedMock = jest.spyOn(actions, 'fetchTopicsFailed');

    test('should dispatch success action with responses', async () => {
        const dispatch = jest.fn();
        getTopicsMockResponseSuccess();

        await fetchTopics()(dispatch);

        expect(fetchTopicsStartMock).toHaveBeenCalledTimes(1);
        expect(fetchTopicsSuccessMock).toHaveBeenCalledWith([basicResponse]);
        expect(fetchTopicsFailedMock).toHaveBeenCalledTimes(0);
    });

    test('should dispatch failed action when request failed', async () => {
        const dispatch = jest.fn();
        getTopicsMockResponseError();

        await fetchTopics()(dispatch);

        expect(fetchTopicsStartMock).toHaveBeenCalledTimes(1);
        expect(fetchTopicsSuccessMock).toHaveBeenCalledTimes(0);
        expect(fetchTopicsFailedMock).toHaveBeenCalledTimes(1);
    });
});
