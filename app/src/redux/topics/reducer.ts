import { ITopic } from '../../types/topic';
import { IFetchingState } from '../types';
import {
    IFetchTopicsActionSuccess,
    IFetchTopicsFailed,
    TopicActions,
    TopicsActionsTypes,
} from './actions';

const initialState: IFetchingState<ITopic[]> = {
    loading: false,
    data: [],
    error: null,
};

const topicsReducer = (state = initialState, action: TopicActions) => {
    switch (action.type) {
        case TopicsActionsTypes.FETCH_TOPICS:
            return {
                ...state,
                loading: true,
            };
        case TopicsActionsTypes.FETCH_TOPICS_SUCCESS: {
            const { topics } = action as IFetchTopicsActionSuccess;
            return {
                ...state,
                data: [...topics],
                loading: false,
            };
        }
        case TopicsActionsTypes.FETCH_TOPICS_FAILED: {
            const { error } = action as IFetchTopicsFailed;
            return {
                ...state,
                loading: false,
                error: error,
            };
        }
        default:
            return state;
    }
};

export default topicsReducer;
