import { Dispatch } from 'redux';
import { ITopic } from '../../types/topic';
import { getTopics } from '../../utils/unsplash';
import {
    IFetchTopicsAction,
    IFetchTopicsActionSuccess,
    IFetchTopicsFailed,
    TopicsActionsTypes,
} from './actions';

export const fetchTopicsStart = (): IFetchTopicsAction => ({
    type: TopicsActionsTypes.FETCH_TOPICS,
});

export const fetchTopicsSuccess = (
    topics: ITopic[],
): IFetchTopicsActionSuccess => ({
    type: TopicsActionsTypes.FETCH_TOPICS_SUCCESS,
    topics,
});

export const fetchTopicsFailed = (error: Error): IFetchTopicsFailed => ({
    type: TopicsActionsTypes.FETCH_TOPICS_FAILED,
    error,
});

export const fetchTopics = () => async (dispatch: Dispatch) => {
    dispatch(fetchTopicsStart());

    try {
        const topicsResponse = await getTopics();
        const topics: ITopic[] = topicsResponse.response!.results.map(
            (topic) =>
                ({
                    id: topic.id,
                    cover_photo: topic.cover_photo,
                    description: topic.description,
                    slug: topic.slug,
                    title: topic.title,
                } as ITopic),
        );

        dispatch(fetchTopicsSuccess(topics));
    } catch (e) {
        fetchTopicsFailed(e);
    }
};
