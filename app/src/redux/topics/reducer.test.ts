import topicsMock from '../../mocks/topics';
import { ITopic } from '../../types/topic';
import { IFetchingState } from '../types';
import {
    fetchTopicsFailed,
    fetchTopicsStart,
    fetchTopicsSuccess,
} from './action-creators';
import topicsReducer from './reducer';

const initialState: IFetchingState<ITopic[]> = {
    loading: false,
    data: [],
    error: null,
};

const error: Error = new Error('error');

const fetchTopicsAction = () => fetchTopicsStart();
const fetchTopicsSuccessAction = () => fetchTopicsSuccess(topicsMock);
const fetchTopicsFailedAction = () => fetchTopicsFailed(error);

describe('topic reducer test', () => {
    it.each`
        action                      | expectedState
        ${fetchTopicsAction}        | ${{ ...initialState, loading: true }}
        ${fetchTopicsSuccessAction} | ${{ ...initialState, data: [...topicsMock] }}
        ${fetchTopicsFailedAction}  | ${{ ...initialState, error }}
    `(
        '<Topics/> renders Loader depends on selector state',
        ({ action, expectedState }) => {
            const results = topicsReducer(initialState, action());

            expect(results).toEqual({
                ...expectedState,
            });
        },
    );
});
