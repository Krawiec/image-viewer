import { Action } from 'redux';
import { ITopic } from '../../types/topic';

export const TopicsActionsTypes = {
    FETCH_TOPICS: 'FETCH_TOPICS',
    FETCH_TOPICS_SUCCESS: 'FETCH_TOPICS_SUCCESS',
    FETCH_TOPICS_FAILED: 'FETCH_TOPICS_FAILED',
};

export interface IFetchTopicsAction extends Action<string> {}

export interface IFetchTopicsActionSuccess extends Action<string> {
    topics: ITopic[];
}

export interface IFetchTopicsFailed extends Action<string> {
    error: Error;
}

export type TopicActions =
    | IFetchTopicsAction
    | IFetchTopicsActionSuccess
    | IFetchTopicsFailed;
