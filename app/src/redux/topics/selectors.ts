import { IReduxState } from '../types';

export const getTopics = (store: IReduxState) => store.topics.data!;

export const loadingTopics = (store: IReduxState) => store.topics.loading;

export const topicsAlreadyLoaded = (store: IReduxState) =>
    store.topics.data!.length > 0;
