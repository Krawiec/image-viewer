import { RouterState } from 'react-router-redux';
import { Action } from 'redux';
import { PhotoPreview } from '../types/photo';
import { ITopic } from '../types/topic';

export interface INetworkError {
    statusCode: number;
    statusText: string;
    statusMessage?: string;
}

export interface IFetchingState<T = {}> {
    loading: boolean;
    error?: Error | null;
    data?: T;
}

export interface IReduxState {
    topics: IFetchingState<ITopic[]>;
    topicPhotos: IFetchingState<PhotoPreview[]>;
    router: RouterState;
}

export interface IActionWithPayload<T = undefined> extends Action<string> {
    payload?: T;
    error?: Error | null;
}
