import { History } from 'history/index';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { IReduxState } from './types';
import topicsReducer from './topics/reducer';
import topicPhotosReducer from './topic-photos/reducer';

const combinedRootReducer = (history: History) =>
    combineReducers<IReduxState>({
        topics: topicsReducer,
        topicPhotos: topicPhotosReducer,
        router: connectRouter(history),
    });

export default combinedRootReducer;
