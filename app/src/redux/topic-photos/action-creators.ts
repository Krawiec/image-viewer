import { Action, Dispatch } from 'redux';
import { OrderBy } from 'unsplash-js';
import { PhotoPreview } from '../../types/photo';
import { getTopicPhotos } from '../../utils/unsplash';
import {
    IFetchTopicPhotoFailed,
    IFetchTopicPhotoSuccessAction,
    TopicPhotosActionTypes,
} from './actions';

export const clearResults = (): Action => ({
    type: TopicPhotosActionTypes.CLEAR_RESULTS,
});

const fetchTopicPhotos = (): Action => ({
    type: TopicPhotosActionTypes.FETCH_TOPIC_PHOTO,
});

const fetchTopicPhotosSuccess = (
    photos: PhotoPreview[],
    resetPreviousResults?: boolean,
): IFetchTopicPhotoSuccessAction => ({
    type: TopicPhotosActionTypes.FETCH_TOPIC_PHOTO_SUCCESS,
    photos,
    resetPreviousResults,
});

const fetchTopicPhotosFailed = (error: Error): IFetchTopicPhotoFailed => ({
    type: TopicPhotosActionTypes.FETCH_TOPIC_PHOTO_FAILED,
    error,
});

export const fetchPhotos = (
    slugOrId: string,
    page: number,
    orderBy: OrderBy,
) => async (dispatch: Dispatch) => {
    dispatch(fetchTopicPhotos());

    try {
        const response = await getTopicPhotos(slugOrId, { page, orderBy });
        const photos: PhotoPreview[] = response.response!.results.map(
            (photo) => ({
                height: photo.height,
                id: photo.id,
                likes: photo.likes,
                urls: photo.urls,
                width: photo.width,
            }),
        );

        dispatch(fetchTopicPhotosSuccess(photos, page === 0));
    } catch (e) {
        dispatch(fetchTopicPhotosFailed(e));
    }
};
