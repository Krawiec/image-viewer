import { PhotoPreview } from '../../types/photo';
import { IFetchingState } from '../types';
import {
    IFetchTopicPhotoFailed,
    IFetchTopicPhotoSuccessAction,
    TopicPhotosActionTypes,
    TopicsActions,
} from './actions';

const initialState: IFetchingState<PhotoPreview[]> = {
    loading: false,
    data: [],
    error: null,
};

const topicPhotosReducer = (state = initialState, action: TopicsActions) => {
    switch (action.type) {
        case TopicPhotosActionTypes.CLEAR_RESULTS:
            return {
                ...state,
                data: [],
                error: null,
                loading: true,
            };
        case TopicPhotosActionTypes.FETCH_TOPIC_PHOTO:
            return {
                ...state,
                loading: true,
            };
        case TopicPhotosActionTypes.FETCH_TOPIC_PHOTO_SUCCESS: {
            const {
                photos,
                resetPreviousResults,
            } = action as IFetchTopicPhotoSuccessAction;
            return {
                ...state,
                data: resetPreviousResults
                    ? [...photos]
                    : [...state.data!, ...photos],
                loading: false,
            };
        }
        case TopicPhotosActionTypes.FETCH_TOPIC_PHOTO_FAILED: {
            const { error } = action as IFetchTopicPhotoFailed;
            return {
                ...state,
                loading: false,
                error: error,
            };
        }
        default:
            return state;
    }
};

export default topicPhotosReducer;
