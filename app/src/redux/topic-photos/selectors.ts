import { IReduxState } from '../types';

export const getTopicPhotos = (state: IReduxState) => state.topicPhotos.data;

export const fetchingTopicPhotos = (state: IReduxState) =>
    state.topicPhotos.loading;
