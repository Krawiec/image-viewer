import { OrderBy } from 'unsplash-js';
import { PhotoPreview } from '../../types/photo';

export interface ITopicPhotos {
    page: number;
    orderBy: OrderBy;
    photos: PhotoPreview[];
}
