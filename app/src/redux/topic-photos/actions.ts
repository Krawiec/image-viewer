import { Action } from 'redux';
import { PhotoPreview } from '../../types/photo';

export const TopicPhotosActionTypes = {
    FETCH_TOPIC_PHOTO: 'FETCH_TOPIC_PHOTO',
    FETCH_TOPIC_PHOTO_SUCCESS: 'FETCH_TOPIC_PHOTO_SUCCESS',
    FETCH_TOPIC_PHOTO_FAILED: 'FETCH_TOPIC_PHOTO_FAILED',
    CLEAR_RESULTS: 'CLEAR_RESULTS',
};

export interface IFetchTopicPhotoSuccessAction extends Action<string> {
    photos: PhotoPreview[];
    resetPreviousResults?: boolean;
}

export interface IFetchTopicPhotoFailed extends Action<string> {
    error: Error;
}

export type TopicsActions =
    | IFetchTopicPhotoSuccessAction
    | IFetchTopicPhotoFailed
    | Action<string>;
