const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.config');
const webpack = require('webpack');

require('dotenv').config();

module.exports = merge(commonConfig, {
    mode: 'production',
    plugins: [
        new webpack.DefinePlugin({
            'process.env.CLIENT_ID': JSON.stringify(process.env.CLIENT_ID)
        })
    ]
});
