module.exports = {
    parser: "@typescript-eslint/parser",
    env: {
        es6: true,
        node: true,
        jest: true
    },
    extends: [
        "eslint:recommended", "plugin:@typescript-eslint/recommended"
    ],
    plugins: ['@typescript-eslint'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module'
    },
    rules: {
        "quotes": [2, "single", { "avoidEscape": true }],
        'class-methods-use-this': 'off',
        'no-param-reassign': 'off',
        camelcase: 'off',
        "no-unused-declaration": "off",
        "no-unused-variable": "off",
        "no-unused-imports": "off",
        'no-unused-vars': "off",
        "no-underscore-dangle": "off",
        "@typescript-eslint/no-non-null-asserted-optional-chain": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "react/require-default-props": "off",
        "react/prop-types": "off",
        "@typescript-eslint/no-empty-function": "off"
    }
};